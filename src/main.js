import 'babel-polyfill'
import 'whatwg-fetch'
import Vue from 'vue'
import vuetify from './plugins/vuetify';
import {ValidationProvider} from 'vee-validate';
import App from './App.vue'
import {router} from '@router'
import {store} from '@store'
import i18n from './services/i18n';
import VueCookies from 'vue-cookies';
// setup fake backend
import {configureFakeBackend} from '@helper';

Vue.component('ValidationProvider', ValidationProvider);
Vue.use(VueCookies);

configureFakeBackend();

Vue.config.productionTip = false

new Vue({
	i18n,
	router,
	store,
	vuetify,
	render: h => h(App)
}).$mount('#app')
