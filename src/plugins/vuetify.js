import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import ES from 'vuetify/es5/locale/es';
import EN from 'vuetify/es5/locale/en';
import PT from 'vuetify/es5/locale/pt';

Vue.use(Vuetify);

export default new Vuetify({
	theme: {
		options: {
			customProperties: true,
		},
		themes: {
			light: {
				primary: '#275c71',
				secondary: '#60c5c6',
				accent: '#2d91ba',
				error: '#b00020',
				success: '#3c9b40',
				warning: '#eaa900',
			},
		},
	},
	lang: {
		locales: {ES, EN, PT},
		current: 'ES',
	},
	icons: {
		iconfont: 'md',
	},
});
