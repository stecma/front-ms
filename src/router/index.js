import Vue from 'vue';
import Router from 'vue-router';

import HomePage from '../pages/HomePage'
import LoginPage from '../pages/LoginPage'
import BlankPage from '../pages/BlankPage'
import MasterPage from '../pages/admin/MasterPage'
import ServicesPage from '../pages/admin/ServicesPage'
import GroupsPage from '../pages/admin/GroupsPage'
import UsersPage from '../pages/admin/UsersPage'
import MenuPage from '../pages/admin/MenuPage'
import CredentialPage from '../pages/credential/CredentialPage'
import MasterCredentialPage from '../pages/credential/MasterCredentialPage'
import SupplierCredentialPage from '../pages/credential/SupplierCreationPage'
import SupplierFilterCredentialPage from '../pages/credential/SupplierFilterPage'
import SupplierMatchPage from '../pages/credential/SupplierMatchPage'
import CustomerMatchPage from '../pages/credential/CustomerMatchPage'
import MassiveTradePage from '../pages/credential/MassiveTradePage'
import ReleasePage from '../pages/credential/ReleasePage'
import CurrencySettings from '../pages/credential/CurrencySettingsPage'

Vue.use(Router);

export const router = new Router({
	mode: 'history',
	routes: [
		{ path: '/login', component: LoginPage },
		{
			path: '/', component: HomePage,
			children: [
				{ path: '/', component: BlankPage, alias: '/logo' },
				{ path: '/api/v1/service', component: ServicesPage, alias: '/admin/service' },
				{ path: '/api/v1/group', component: GroupsPage, alias: '/admin/group' },
				{ path: '/api/v1/user', component: UsersPage, alias: '/admin/user' },
				{ path: '/api/v1/menu', component: MenuPage, alias: '/admin/menu' },
				{ path: '/api/v1/master', component: MasterPage, alias: '/admin/master' },
				{ path: '/api/v1/credential', component: CredentialPage, alias: '/credential' },
				{ path: '/api/v1/master/credential/:entity', component: MasterCredentialPage, alias: '/master/credential' },
				{ path: '/api/v1/supplier', component: SupplierCredentialPage, alias: '/supplier' },
				{ path: '/api/v1/supplier/filter', component: SupplierFilterCredentialPage, alias: '/supplier/filter' },
				{ path: '/api/v1/match/supplier', component: SupplierMatchPage, alias: '/match/supplier' },
				{ path: '/api/v1/match/customer', component: CustomerMatchPage, alias: '/match/customer' },
				{ path: '/api/v1/trades/massive', component: MassiveTradePage, alias: '/trades/massive' },
				{ path: '/api/v1/release', component: ReleasePage, alias: '/release' },
				{ path: '/api/v1/currency/bycountry', component: CurrencySettings, alias: '/currency/bycountry' }
			]
		},
		// otherwise redirect to home
		{ path: '*', redirect: '/' }
	]
});

router.beforeEach((to, from, next) => {
	// redirect to login page if not logged in and trying to access a restricted page
	const publicPages = ['/login'];
	const authRequired = !publicPages.includes(to.path);
	const loggedIn = localStorage.getItem('user');

	if (authRequired && !loggedIn) {
		return next('/login');
	}

	next();
})