export const masterUtils = {
	getEntityValues,
	getMasters,
	getCode,
	getCodes,
	getMasterFromCodes
};

function getEntityValues(masters, child) {
	let elements;
	if (child === "brand") {
		elements = masters.brands;
	} else if (child === "depth") {
		elements = masters.depths;
	} else if (child === "service_type") {
		elements = masters.serviceTypes;
	} else if (child === "distribution_model" || child === "model" || child === "distmodel_from_servicetype") {
		elements = masters.distributionModels;
	} else if (child === "request") {
		elements = masters.requests;
	} else if (child === "environments") {
		elements = masters.envCredential;
	} else if (child === "platform") {
		elements = masters.platforms;
	} else if (child === "product_origin") {
		elements = masters.productOrigins;
	} else if (child === "sales_model") {
		elements = masters.salesModels;
	} else if (child === "sales_channel" || child === 'slc') {
		elements = masters.salesChannels;
	} else if (child === "rate_type" || child === "rate" || child === "rate_from_servicetype") {
		elements = masters.rateTypes;
	} else if (child === "country" || child === 'countries' || child === "exccountry") {
		elements = masters.countries;
	} else if (child === "service_distribution") {
		elements = masters.distributionServices;
	} else if (child === "users") {
		elements = masters.users;
	} else if (child === "languages") {
		elements = masters.languages;
	} else if (child === "sys_supplier") {
		elements = masters.syssuppliers;
	} else if (child === 'chain') {
		elements = masters.chains;
	} else if (child === 'transfer_service_type') {
		elements = masters.transferServiceTypes;
	} else if (child === 'transfer_type') {
		elements = masters.transferTypes;
	} else if (child === 'activity_service_type') {
		elements = masters.activityServiceTypes;
	} else if (child === 'activity_segmentation') {
		elements = masters.activitySegmentations;
	} else if (child === 'segmentation') {
		elements = masters.segmentations;
	} else if (child === 'area') {
		elements = masters.areas;
	} else if (child === 'mainland') {
		elements = masters.mainlands;
	} else if (child === 'state') {
		elements = masters.states;
	} else if (child === 'sale_type') {
		elements = masters.saleTypes;
	} else if (child === "corp" || child === "corporation") {
		elements = masters.corporations;
	} else if (child === "external_system") {
		elements = masters.externalSystems;
	} else if (child === "sub_supplier") {
		elements = masters.subsuppliers;
	} else if (child === "buy_model") {
		elements = masters.buyModels;
	} else if (child === "client_pref") {
		elements = masters.clientPrefs;
	} else if (child === "currencies" || child === 'currency') {
		elements = masters.currencies;
	} else if (child === "factype" || child === "billing_type") {
		elements = masters.invoicetypes;
	} else if (child === "top_destination") {
		elements = masters.topDestinations;
	} else if (child === "status_credential") {
		elements = masters.statusCredentials;
	} else if (child === "clienttype" || child === "client_type") {
		elements = masters.clienttypes;
	} else if (child === 'comercial_area') {
		elements = masters.comercialareas;
	} else if (child === 'agency_group') {
		elements = masters.agencygroups;
	} else if (child === 'payment') {
		elements = masters.payments;
	} else if (child === 'amadeus') {
		elements = masters.amadeus;
	} else if (child === 'num_clients') {
		elements = masters.numClients;
	}

	let values = [];
	if (elements) {
		for (let element of elements) {
			if (element.act) {
				values.push(element);
			}
		}
	}
	return values;
}

function getMasters(masters, entity, multiple) {
	let rows = null;
	if (masters) {
		rows = [];
		for (let select of masters) {
			if (select.entity.toLowerCase() === entity) {
				rows.push(select);
			}
		}

		if (!multiple) {
			return rows[0];
		}
	}
	return rows;
}

function getCode(codes, entity) {
	const rows = this.getCodes(codes, entity, false);
	return rows ? rows : "";
}

function getCodes(codes, entity, multiple) {
	let rows = null;
	if (codes) {
		rows = [];
		for (let select of codes) {
			if (select.key.toLowerCase() === entity) {
				rows.push(...select.value);
			}
		}

		if (!multiple) {
			return rows[0];
		}
	}
	return rows;
}

function getMasterFromCodes(allMasters, codes, entity) {
	if (codes) {
		const code = this.getCodes(codes, entity, false);
		if (code) {
			const masters = this.getEntityValues(allMasters, entity, true);
			if (masters) {
				for (let master of masters) {
					if (master.id === code) {
						return master;
					}
				}
			}
		}
	}
	return null;
}