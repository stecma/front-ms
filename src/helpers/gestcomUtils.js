import { utils } from "./utils";
import { masterUtils } from "./masterUtils";

export const gestcomUtils = {
	validateGroups,
	updateCredentialKeys,
	calculateClientName,
	isHotelSelected,
	isApiSelected,
	isTravelGateSelected,
	isNewTowrSelected,
	isWebSelected,
	isModelCPQ,
	isModelCHO,
	findCustomer
};

function isHotelSelected(credential){
  return credential.srvtype && credential.srvtype.id === 'HTL';
}

function isApiSelected(credential){
	return credential.salesChannel && credential.salesChannel.id === 'API';
}

function isNewTowrSelected(credential){
	return credential.salesChannel && credential.salesChannel.id === 'NWW';
}

function isWebSelected(credential){
	return credential.salesChannel && credential.salesChannel.id === 'WEB';
}

function isTravelGateSelected(credential){
    return credential.platforms && credential.platforms.length === 1 && credential.platforms[0].id === 'XTG';
}

function isModelCHO(credential) {
	return credential.model && credential.model.id === 'CHO';
}

function isModelCPQ(credential) {
	return credential.model && credential.model.id === 'CPQ';
}

function validateGroups(newGroup,groups) {
	let msg = "";
	if(groups && groups.length > 0){
		groups.forEach(element => {
			if(sameDescription(newGroup.sysname,element.sysname) && newGroup.id !== element.id){
				msg =  "INVALID_DESC";
				return;
			}else{
				var groupId = sameConfiguration(newGroup,element);
				if(groupId !== ""){
					msg = groupId;
					return;
				}
			}
		});
	}
	return msg;
}

function updateCredentialKeys(credential,client, uniqueClient) {
	
	let brandData = getBrandData(credential);
	let serviceType = getServiceType(credential);
	let clientData =  getClientData(client,uniqueClient);
	let distributionModel = getDistributionModel(credential);
	let salesChannel = getSalesChannel(credential);
	let salesModel = getSalesModel(credential);
	let market = getMarket(credential);
	let currency = getCurrency(credential);
	let demoTest = getDemoTest(credential);
	
	return setCredentialKeys(credential,brandData,distributionModel,serviceType,salesChannel,salesModel,market,currency,demoTest,clientData);
}

function calculateClientName(client, uniqueClent) {
	return uniqueClent ? getUniqueClientName(client): "";	
}

function getUniqueClientName(client) {
	let name = "";
	if (client) {
		if (client.grp) {
          name = utils.valueLanguage(client.grp.name, "ESP");
        }else{
          if(client.age){
            name = utils.valueLanguage(client.age.name, "ESP");
          }
          if(client.branch){
            name = name.concat(" - ").concat(utils.valueLanguage(client.branch.name, "ESP"));
          }
          if(client.agent){
            name = name.concat(" - ").concat(utils.valueLanguage(client.agent.name, "ESP"));
          }
        }
	}
	return name;
}

function getUniqueClientAliasAndId(client) {
	let aliasAndId = "";
	if (client) {
		if (client.grp) {
          aliasAndId = "G-".concat(client.grp.id.substring(0,3)).concat("_");
        }else{
          if(client.age){
            aliasAndId = client.age.id.concat("_");
          }
          if(client.branch){
           aliasAndId = client.age.id.substring(0,2).concat(client.branch.id.substring(0,3)).concat("_");
          }
          if(client.agent){
            aliasAndId = client.age.id.substring(0,1).concat(client.branch.id.substring(0,3)).concat("A_");
          }
        }
	}
	return aliasAndId;
}

function setCredentialKeys(credential, brandData, distributionModel, serviceType, salesChannel, salesModel, market,currency,demoTest,clientData) {
	let alias = "";
	let desc = "";
	let id = "";
	if (brandData) {
		alias = alias.concat(brandData.alias);
		desc = desc.concat(brandData.desc);	
	}

	if (serviceType) {
		alias = alias.concat(serviceType.alias);
		desc = desc.concat(serviceType.desc);	
	}

	if (clientData) {
		alias = alias.concat(clientData.alias);
		desc = desc.concat(clientData.desc);
		id = id.concat(clientData.id);
	}

	if (distributionModel) {
		alias = alias.concat(distributionModel.alias);
		desc = desc.concat(distributionModel.desc);
		id = id.concat(distributionModel.id);
	}

	if (salesChannel) {
		alias = alias.concat(salesChannel.alias);
		desc = desc.concat(salesChannel.desc);	
	}

	if (salesModel) {
		alias = alias.concat(salesModel.alias);
		desc = desc.concat(salesModel.desc);	
	}

	if (market) {
		alias = alias.concat(market.alias);
		desc = desc.concat(market.desc);	
	}

	if (currency) {
		alias = alias.concat(currency.alias);
		desc = desc.concat(currency.desc);	
	}
	if (demoTest) {
		alias = alias.concat(demoTest.alias);
		desc = desc.concat(demoTest.desc);	
	}
	credential.alias = alias;
	credential.desc = desc;
	credential.alias = alias;
	credential.desc = desc;
	if (credential.systemIdInDB === '') {
		credential.systemId = id;
	}

	return credential;
}

function getBrandData(credential) {
	let brandData = null;
	if (credential.brand && credential.brand.id !== '') {
		let value = credential.brand.id;
		
		brandData = {
			desc: value === 'MAR' ? 'MRS_' : (value === 'WIS' ? 'WIS_' : ''),
			alias: value === 'MAR' ? 'M_' : (value === 'WIS' ? 'W_' : '')
		};
	}
	return brandData;
}

function getServiceType(credential) {
	let serviceData = null;
	if (credential.srvtype && credential.srvtype.id !== '') {
		switch (credential.srvtype.id) {
			case 'HTL':
				serviceData = {
					desc: 'H_',
					alias : 'H_'
				};
				break;
			case 'TRF':
				serviceData = {
					desc: 'T_',
					alias : 'T_'
				};
				break;
			case 'EXP':
				serviceData = {
					desc: 'E_',
					alias : 'E_'
				};
				break;
		}
		
	}
	return serviceData;
}

function getClientData(client,uniqueClient) {
	let clientData = null;
	if (client) {
		clientData = {
			desc: uniqueClient ? getUniqueClientName(client).concat("_") : "",
			alias: uniqueClient ? getUniqueClientAliasAndId(client) : "",
			id: uniqueClient ? getUniqueClientAliasAndId(client) : ""
		};
	}
	
	
	return clientData;
}

function getSalesChannel(credential) {
	let salesChannel = null;
	if (credential.salesChannel && credential.salesChannel.id !== '') {
		switch (credential.salesChannel.id) {
			case 'API':
				salesChannel = {
					desc: 'A_',
					alias : 'A_'
				};
				break;
			case 'WEB':
				salesChannel = {
					desc: 'W_',
					alias : 'W_'
				};
				break;
			case 'NWW':
				salesChannel = {
					desc: 'N_',
					alias : 'N_'
				};
				break;
		}
		
	}
	return salesChannel;
}

function getSalesModel(credential) {
	let salesModel = null;
	if (credential.salesModel && credential.salesModel.id !== '') {
		switch (credential.salesModel.id) {
			case 'N':
				salesModel = {
					desc: 'NET_',
					alias : 'N_'
				};
				break;
			case 'C':
				salesModel = {
					desc: 'COM_',
					alias : 'C_'
				};
				break;
			case 'X':
				salesModel = {
					desc: 'N+C_',
					alias : 'X_'
				};
				break;
		}
	}
	return salesModel;
}

function getMarket(credential) {
	let market = null;
	if (credential.market) {
		let arrName = credential.market.name[0].value.split("] ");
		market = {
			desc: arrName[1].concat("_"),
			alias: credential.market.id.concat("_")
		};

	} else if (credential.tagnac) {
		market = {
			desc: 'TAGNAC_',
			alias: 'XX_'
		};
	} else {
		market = {
			desc: '00_',
			alias: '-_'
		};
	}
	return market;
}

function getCurrency(credential) {
	let currency = null;
	if (credential.cur) {
		if (credential.cur.id === 'MULTI') {
			currency = {
				desc: "MDV_",
				alias: "MDV_"
			};
		} else {
			currency = {
				desc: credential.cur.id.concat("_"),
				alias: credential.cur.id.concat("_")
			};
		}
	} else {
		currency = {
			desc: "CLI_",
			alias: "CLI_"
		};
	}
	return currency;
}

function getDemoTest(credential) {
	let demoTest = null;
	if (credential.demo) {
		demoTest = {
				desc: "DEMO_",
				alias:"D_"
			};
	} else if (credential.test) {
		demoTest = {
				desc: "TEST_",
				alias: "T_"
			};
	}
	return demoTest;
}

function getDistributionModel(credential) {
	let distributionModel = null;
	if (credential.model && credential.srvtype && credential.srvtype.id === 'HTL') {
		let arrName = credential.model.name[0].value.split("] ");
			distributionModel = {
				desc: arrName[1].concat("_"),
				alias: credential.model.id.concat("_"),
				id: credential.model.id.concat("_")
			};
			
	} else if (credential.srvtype && credential.srvtype.id === 'TRF') {
		distributionModel = {
			desc: 'TRASLADOS_',
				alias: 'T_',
				id: 'TRF_',
			};
	} else if (credential.srvtype && credential.srvtype.id === 'EXP') {
		distributionModel = {
			desc: 'EXPERIENCIAS_',
			alias: 'EXP_',
			id: 'EXP_'
		};
	}	
	return distributionModel;
}


function sameDescription(newGroupDesc,elementDesc){
	return newGroupDesc === elementDesc;
}

function sameConfiguration(newGroup,element){
	let sameConfig = newGroup.dft === element.dft && 
	newGroup.act === element.act &&
	newGroup.test === element.test &&
	newGroup.hotx === element.hotx &&
	newGroup.brand.id === element.brand.id && 
	newGroup.clientype.id === element.clientype.id &&
	newGroup.del === element.del &&
	sameDivs(newGroup,element) &&
	sameNac(newGroup,element)
	sameElementsInArray(newGroup.models,element.models) &&
	sameElementsInArray(newGroup.rates,element.rates);
	
	return sameConfig && newGroup.id !== element.id ? element.sysname : "";
}

function sameDivs(newGroup,element){
	return newGroup.tagdiv === element.tagdiv ? true : sameElementsInArray(newGroup.divs,element.divs);
}

function sameNac(newGroup,element){
	return newGroup.tagnac === element.tagnac ? true : 
		sameElementsInArray(newGroup.cous,element.cous) && sameElementsInArray(newGroup.exccous,element.exccous);
}

function sameElementsInArray(array1,array2){
	let length1 = !array1 ? 0 : array1.length;
	let length2 = !array2 ? 0 : array2.length;
	let count = 0;
	if(length1 === 0 && length2 === 0){
		return true;
	}else if(length1 !== length2){
		return false;
	}else{
		for(let i = 0; i < length1; i++){
			for(let j = 0; j < length2; j++){
				if(array1[i].id === array2[j].id){
					count++;
					break;
				}
			}
		}
		return count === length1;
	}

}



function findCustomer(codes,master){
	let group = null;
	let agency = null;
	let agent = null;
	let branch = null;
	let customer = null;
    for(let code of codes){
		if(code.entity === "AGENCY_GROUP"){
			group = getAgencyGroup(master, code.id);
		}
		if (code.entity === "AGENCY") {
			
			agency = code;
		}
		if (code.entity === "BRANCH_OFFICE") {
			branch = code;
        }
		if(code.entity === "AGENT"){
			agent = code;
        }
	}
	customer = {
		grp: group,
		age: agency,
		agent: agent,
		branch: branch
	};
      return customer;
}

function getAgencyGroup(master,id) {
    const elements = masterUtils.getEntityValues(master, 'agency_group');
    let row = null;
	for (let element of  elements) {
		if (element.id === id) {
			row = element;
			break;
		}
    } 
    return row;
}