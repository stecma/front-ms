export const dateUtils = {
	formatDate,
	parseDate,
	overlap,
	validRangeDate,
	isDateBetween,
	getCurrentDate,
	getCurrentTime,
	getMinAllowedDate,
	isDateLessThanNow,
	stringToDate
};

function formatDate(date) {
	let formatDate = date;
	if (date && !date.includes("/")) {
		const [year, month, day] = date.split("-");
		formatDate = `${day.padStart(2, "0")}/${month.padStart(2, "0")}/${year}`;
	}
	return formatDate;
}

function parseDate(date) {
	let parseDate = date;
	if (date && !date.includes("-")) {
		const [day, month, year] = date.split("/");
		if(!day || !month || !year ){
			parseDate = null;
		}else{
			parseDate = `${year}-${month.padStart(2, "0")}-${day.padStart(2, "0")}`;
		}
		
	}
	return parseDate;
}

function overlap(dates, overlap) {
	const date1 = this.validRangeDate(this.parseDate(dates.date1.from), this.parseDate(dates.date1.to), dates.date1.timefrom, dates.date1.timeto, overlap);
	const date2 = this.validRangeDate(this.parseDate(dates.date2.from), this.parseDate(dates.date2.to), dates.date2.timefrom, dates.date2.timeto, overlap);
	return date1 && date2;
}

function validRangeDate(date1, date2, time1, time2, overlap) {
	let startDate;
	if (!date1) {
		startDate = new Date();
	} else {
		startDate = Date.parse(date1 + " " + time1);
	}

	let endDate;
	if (!date2) {
		endDate = new Date(2050, 12, 31, 23, 59, 59);
	} else {
		endDate = Date.parse(date2 + " " + time2);
	}

	if (overlap) {
		return endDate >= startDate;
	}
	return endDate > startDate;
}

function isDateBetween(date, dateFrom, dateTo) {
	return dateFrom <= date && dateTo >= date;
}

function getCurrentDate() {
	return this.formatDate(this.parseDate(new Date().toLocaleDateString()));
}

function getCurrentTime() {
	const currentDate = new Date();
	const hour = currentDate.getHours().toString();
	const minutes = currentDate.getMinutes().toString();
	const seconds = currentDate.getSeconds().toString();

	return hour.padStart(2, "0").concat(":").concat(minutes.padStart(2, "0")).concat(":").concat(seconds.padStart(2, "0"));
}

function getMinAllowedDate() {
	return new Date().toISOString().slice(0,10);
}

function isDateLessThanNow(dateString){
	const [day, month, year] = dateString.split("/");
	if(!day || !month || !year ){
		return false;
	}
	const compareDate = new Date(year,month-1,day);
	const currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
	const greatherOrEquals = compareDate < currentDate;
	return greatherOrEquals;
}

function stringToDate(date) {
	var parts =this.parseDate(date).split('-');
	return new Date(parts[0], parts[1] - 1, parts[2]);
}