export function configureFakeBackend() {
	let realFetch = window.fetch;
	window.fetch = function (url, opts) {
		return new Promise((resolve, reject) => {
			// wrap in timeout to simulate server api call
			setTimeout(() => {

				// authenticate
				if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
					// get parameters from post request
					let params = JSON.parse(opts.body);
					if (params.username === "admin" && params.password === "admin") {
						let responseJson = {
							id: '1',
							username: 'admin',
							firstName: 'Julio',
							lastName: 'Varela',
							token: 'fake-jwt-token'
						};
						resolve({ok: true, text: () => Promise.resolve(JSON.stringify(responseJson))});
					} else {
						// else return error
						reject('Username or password is incorrect');
					}
					return;
				}

				// pass through any requests not handled above
				realFetch(url, opts).then(response => resolve(response));

			}, 500);
		});
	}
}