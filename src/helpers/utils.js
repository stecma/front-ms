import i18n from "../services/i18n";

export const utils = {
	valueAppLanguage,
	valueLanguage,
	valueUrl,
	isActive,
	isExternal,
	isForcedIE,
	isVisible,
	isAdmin,
	getColor,
	getLogo,
	getIndexOf,
	logout,
	getResponse
};

function valueAppLanguage(names) {
	let code = i18n.locale;
	for (let name of names) {
		if (name.key.length > code.length) {
			code = "ESP";
			break;
		}
	}
	return this.valueLanguage(names, code);
}

function valueLanguage(names, code) {
	if (names) {
		for (let name of names) {
			if (name.key === code) {
				return name.value
			}
		}
	}
	
	return "";
}

function valueUrl(item) {
	let res = "";
	if (item != null && item.url != null) {
		res = item.url;
		if (item.params != null) {
			let params = "";
			for (let param of item.params) {
				if (params.length > 0) {
					params += '&';
				}
				params += param.key + '=' + param.value;
			}
			res += params;
		}
	}
	return res;
}

function isActive(active) {
	let res = "tables.enabled";
	if (!active) {
		res = "tables.disabled";
	}
	return res;
}

function isExternal(external) {
	let res = "tables.internal";
	if (external) {
		res = "tables.external";
	}
	return res;
}

function isForcedIE(forcedIE) {
	let res = "";
	if (forcedIE) {
		res = "tables.navigator";
	}
	return res;
}

function isVisible(visible) {
	let res = "";
	if (visible) {
		res = "tables.visible";
	}
	return res;
}

function isAdmin(admin) {
	let res = "";
	if (admin) {
		res = "tables.admin";
	}
	return res;
}

function getColor(value) {
	let color = null;
	if (value !== "") {
		color = "green";
		if (value.includes("internal") || value.includes("LANGUAGE")) {
			color = "primary";
		}
		if (value.includes("disabled") || value.includes("navigator") || value.includes("COMPANY")) {
			color = "red";
		}
		if (value.includes("BUSINESS_APP")) {
			color = "grey";
		}
	}
	return color;
}

function getLogo(cia, cias) {
	let logo = "default.jpg";
	if (cias !== null) {
		for (let element of cias) {
			if (element.id === cia && element.logo !== "") {
				logo = element.logo;
				break;
			}
		}
	}
	return logo;
}

function getIndexOf(rows, id) {
	for (let i = 0; i < rows.length; i++) {
		if (id === rows[i].id || id === rows[i].user || id === rows[i].key || id === rows[i]) {
			return i;
		}
	}
	return -1;
}

function logout() {
	// remove user from local storage to log user out
	localStorage.removeItem('user');
	localStorage.removeItem('bearer');
}

function getResponse(text, response, addAuth) {
	const data = text && JSON.parse(text);
	if (!response.ok) {
		if (response.status === 401) {
			// auto logout if 401 response returned from api
			logout();
			location.reload();
		} else {
			const error = (data && data.response && data.response.messages[0]) || response.statusText || data.message;
			return Promise.reject(error);
		}
	}

	if (addAuth) {
		localStorage.setItem('bearer', response.headers.get("authorization"));
	}
	return data;
}