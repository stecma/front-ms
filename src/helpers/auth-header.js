export function authHeader() {
	// return authorization header with jwt token
	let user = JSON.parse(localStorage.getItem('user'));
	let token = localStorage.getItem('bearer');

	if (token) {
		return {'Authorization': token};
	} else {
		if (user && user.token) {
			return {'Authorization': user.token};
		} else {
			return {};
		}
	}
}