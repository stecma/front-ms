import { authHeader } from "./auth-header";
import { utils } from "./utils";

export const rest = {
	handleRequest
};

async function handleRequest(apiRoute, store, method, body) {
	const requestOptions = {
		method: method,
		headers: { 'Content-Type': 'application/json', ...authHeader() },
		body: body
	};

	console.log(apiRoute.concat(" Method: ").concat(method).concat(" Request: ").concat(body));
	const response = await fetch(process.env.VUE_APP_API.concat(apiRoute), requestOptions);
	const apiRS = await handleResponse(response);
	if (store !== null && apiRS.response && apiRS.response.length > 0) {
		localStorage.setItem(store, JSON.stringify(apiRS.response));
	}
	return apiRS.response;
}

function handleResponse(response) {
	return response.text().then(text => utils.getResponse(text, response, true));
}