import {rest} from "@helper";

export const gestcom = {
	getItem,
	getAllItems,
	match,
	getPermission,
	assignment,
	massive,
	create,
	update,
	remove,
	getIcon,
	getIconColor
};

function getItem(route, body) {
	return rest.handleRequest(route, null, "POST", body);
}

function getAllItems(route, body) {
	return rest.handleRequest(route.concat('/all'), null, "POST", body);
}

function match(route, body) {
	return rest.handleRequest(route, null, "POST", body);
}

function massive(route, body) {
	return rest.handleRequest(route, null, "POST", body);
}

function getPermission(body) {
	return rest.handleRequest("/api/v1/permission", null, "POST", body);
}

function assignment(body) {
	return rest.handleRequest("/api/v1/match/assign", null, "POST", body);
}

function create(route, body) {
	return rest.handleRequest(route.concat("/add"), null, "POST", body);
}

function update(route, body) {
	return rest.handleRequest(route, null, "PUT", body);
}

function remove(route, body) {
	return rest.handleRequest(route, null, "DELETE", body);
}

function getIcon(color) {
	if (!color || color.toLowerCase() === 'red') {
		return 'disabled_by_default';
	} else if (color.toLowerCase() === 'green') {
		return 'check_circle';
	}
	return 'report_problem';
}

function getIconColor(color) {
	if (!color) {
		return 'red';
	}
	if (color.toLowerCase() === 'yellow') {
		return '#ecb538';
	}
	return color;
}
