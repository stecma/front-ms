import Vue from 'vue';
import VueI18n from 'vue-i18n';

import es from '../assets/langs/es';
import en from '../assets/langs/en';
import pt from '../assets/langs/pt';

Vue.use(VueI18n);

const messages = {'ES': es, 'EN': en, 'PT': pt};

const i18n = new VueI18n({
	locale: 'ES', // set locale
	fallbackLocale: 'ES', // set fallback locale
	messages, // set locale messages
});

export default i18n;