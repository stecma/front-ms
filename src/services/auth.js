import { utils } from "@helper/utils";

export const authService = {
	login,
	logout
};

function login(user, pass) {
	const requestOptions = {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({ user, pass })
	};
	return fetch(process.env.VUE_APP_API + '/api/v1/login', requestOptions)
		.then(handleResponse)
		.then(user => {
			// login successful if there's a jwt token in the response
			if (user.response.token) {
				// store user details and jwt token in local storage to keep user logged in between page refreshes
				localStorage.setItem('user', JSON.stringify(user.response));
				localStorage.setItem('bearer', user.response.token);
			}

			return user.response;
		});
}

function logout() {
	utils.logout();
}

function handleResponse(response) {
	return response.text().then(text => utils.getResponse(text, response, false));
}