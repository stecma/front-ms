import { rest } from "@helper";

export const masterSrv = {
	getMaster,
	getServices,
	getGroups,
	getDepths,
	getPlatforms,
	getSalesModels,
	getRateTypes,
	getProductOrigins,
	getRequests,
	getDistributionModels,
	getEnvCredential,
	getLanguages,
	getSalesChannels,
	getServiceTypes,
	getDistributionServices,
	getSystems,
	getUsers,
	getBuyModels,
	getClientPrefs,
	getTopDestinations,
	getCurrencies,
	getCountries,
	getCorporations,
	getInvoiceTypes,
	getBrands,
	getClientTypes,
	getAreas,
	getSubSupplier,
	getState,
	getStatusCredentials,
	getChains,
	getSysSupplier,
	getSegmentations,
	getMainlands,
	getSaleTypes,
	getTransferServiceTypes,
	getTransferTypes,
	getActivityServiceTypes,
	getActivitySegmentations,
	getCommercialAreas,
	getAgencyGroups,
	getPayments,
	getAmadeus,
	getAllItems,
	getNumClients
};

function getMaster(store) {
	const entity = store;
	return getAllItems('/api/v1/master', 'POST', store, JSON.stringify({ entity }));
}

function getServices(env) {
	return getAllItems('/api/v1/service', 'POST', null, JSON.stringify({ env }));
}

function getGroups(env) {
	return getAllItems('/api/v1/group', 'POST', null, JSON.stringify({ env }));
}

function getDepths() {
	return getAllCredentialItems("depth", true);
}

function getPlatforms() {
	return getAllCredentialItems("platform", true);
}

function getSalesModels() {
	return getAllCredentialItems("sales_model", true);
}

function getRateTypes() {
	return getAllCredentialItems("rate_type", true);
}

function getProductOrigins() {
	return getAllCredentialItems("product_origin", true);
}

function getDistributionModels() {
	return getAllCredentialItems("distribution_model", true);
}

function getEnvCredential() {
	return getAllCredentialItems("environments", true);
}

function getRequests() {
	return getAllCredentialItems("request", true);
}

function getLanguages() {
	return getAllCredentialItems("languages", true);
}

function getServiceTypes() {
	return getAllCredentialItems("service_type", true);
}

function getSalesChannels() {
	return getAllCredentialItems("sales_channel", true);
}

function getDistributionServices() {
	return getAllCredentialItems("service_distribution", true);
}

function getSystems() {
	return getAllCredentialItems("external_system", true);
}

function getUsers() {
	return getAllCredentialItems("users", false);
}

function getBuyModels() {
	return getAllCredentialItems("buy_model", true);
}

function getClientPrefs() {
	return getAllCredentialItems("client_pref", true);
}

function getTopDestinations() {
	return getAllCredentialItems("top_destination", true);
}

function getCurrencies() {
	return getAllCredentialItems("currency", false);
}

function getCountries() {
	return getAllCredentialItems("country", false);
}

function getCorporations() {
	return getAllCredentialItems("corporation", true);
}

function getInvoiceTypes() {
	return getAllCredentialItems("billing_type", true);
}

function getBrands() {
	return getAllCredentialItems("brand", true);
}

function getClientTypes() {
	return getAllCredentialItems("client_type", true);
}

function getAreas() {
	return getAllCredentialItems("area", false);
}

function getSubSupplier() {
	return getAllCredentialItems("sub_supplier", false);
}

function getSysSupplier() {
	return getAllCredentialItems("sys_supplier", false);
}

function getState() {
	return getAllCredentialItems("state", false);
}

function getStatusCredentials() {
	return getAllCredentialItems("status_credential", true);
}

function getChains() {
	return getAllCredentialItems("chain", true);
}

function getSegmentations() {
	return getAllCredentialItems("segmentation", true);
}

function getMainlands() {
	return getAllCredentialItems("mainland", true);
}

function getSaleTypes() {
	return getAllCredentialItems("sale_type", true);
}

function getTransferServiceTypes() {
	return getAllCredentialItems("transfer_service_type", true);
}

function getTransferTypes() {
	return getAllCredentialItems("transfer_type", true);
}

function getActivityServiceTypes() {
	return getAllCredentialItems("activity_service_type", true);
}

function getActivitySegmentations() {
	return getAllCredentialItems("activity_segmentation", true);
}

function getCommercialAreas() {
	return getAllCredentialItems("comercial_area", true);
}

function getAgencyGroups() {
	return getAllCredentialItems("agency_group", true);
}

function getPayments() {
	return getAllCredentialItems("payment", true);
}

function getAmadeus() {
	return getAllCredentialItems("amadeus", true);
}

function getNumClients() {
	return getAllCredentialItems("num_clients", true);
}

function getAllCredentialItems(entity, saveCache) {
	let store = null;
	if (saveCache) {
		store = entity;
	}
	return getAllItems('/api/v1/master/credential/'.concat(entity), 'POST', store, JSON.stringify({ entity }));
}

function getAllItems(route, apiMethod, store, body) {
	return rest.handleRequest(route.concat('/all'), store, apiMethod, body);
}