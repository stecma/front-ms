import {rest} from "@helper";

export const admin = {
	getItem,
	getAllItems,
	create,
	update,
	remove,
};

function getItem(route, body) {
	return rest.handleRequest(route, null, "POST", body);
}

function getAllItems(route, body) {
	return rest.handleRequest(route.concat('/all'), null, "POST", body);
}

function create(route, body) {
	return rest.handleRequest(route.concat("/add"), null, "POST", body);
}

function update(route, body) {
	return rest.handleRequest(route, null, "PUT", body);
}

function remove(route, body) {
	return rest.handleRequest(route, null, "DELETE", body);
}