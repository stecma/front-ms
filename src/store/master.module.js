import {masterSrv} from '@services';

const state = {
    business_app: null,
    languages_app: null,
    companies: null,
    environments: null,
    services: null,
    groups: null,
    depths: null,
    platforms: null,
    salesModels: null,
    rateTypes: null,
    productOrigins: null,
    envCredential: null,
    requests: null,
    distributionModels: null,
    languages: null,
    brands: null,
    serviceTypes: null,
    salesChannels: null,
    distributionServices: null,
    externalSystems: null,
    users: null,
    buyModels: null,
    clientPrefs: null,
    topDestinations: null,
    currencies: null,
    countries: null,
    corporations: null,
    invoicetypes: null,
    clienttypes: null,
    areas: null,
    subsuppliers: null,
    syssuppliers: null,
    states: null,
    statusCredentials: null,
    chains: null,
    segmentations: null,
    mainlands: null,
    saleTypes: null,
    transferServiceTypes: null,
    transferTypes: null,
    activityServiceTypes: null,
    activitySegmentations: null,
    comercialareas: null,
    agencygroups: null,
    payments: null,
    amadeus: null,
    numClients: null
};

const actions = {
    getAllCacheWIS({dispatch, commit}) {
        masterSrv.getDepths()
                .then(
                        depths => {
                            commit("depths", depths);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getPlatforms()
                .then(
                        platforms => {
                            commit("platforms", platforms);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getSalesModels()
                .then(
                        salesModels => {
                            commit("salesModels", salesModels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getRateTypes()
                .then(
                        rateTypes => {
                            commit("rateTypes", rateTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getProductOrigins()
                .then(
                        productOrigins => {
                            commit("productOrigins", productOrigins);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getEnvCredential()
                .then(
                        envCredential => {
                            commit("envCredential", envCredential);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getDistributionModels()
                .then(
                        distributionModels => {
                            commit("distributionModels", distributionModels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getRequests()
                .then(
                        requests => {
                            commit("requests", requests);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getLanguages()
                .then(
                        languages => {
                            commit("languages", languages);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getServiceTypes()
                .then(
                        serviceTypes => {
                            commit("serviceTypes", serviceTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getSalesChannels()
                .then(
                        salesChannels => {
                            commit("salesChannels", salesChannels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getDistributionServices()
                .then(
                        distributionServices => {
                            commit("distributionServices", distributionServices);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getSystems()
                .then(
                        externalSystems => {
                            commit("externalSystems", externalSystems);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getUsers()
                .then(
                        users => {
                            commit("users", users);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getBuyModels()
                .then(
                        buyModels => {
                            commit("buyModels", buyModels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getClientPrefs()
                .then(
                        clientPrefs => {
                            commit("clientPrefs", clientPrefs);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getTopDestinations()
                .then(
                        topDestinations => {
                            commit("topDestinations", topDestinations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getCorporations()
                .then(
                        corporations => {
                            commit("corporations", corporations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getInvoiceTypes()
                .then(
                        invoicetypes => {
                            commit("invoicetypes", invoicetypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getBrands()
                .then(
                        brands => {
                            commit("brands", brands);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getClientTypes()
                .then(
                        clienttypes => {
                            commit("clienttypes", clienttypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getStatusCredentials()
                .then(
                        statusCredentials => {
                            commit("statusCredentials", statusCredentials);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getChains()
                .then(
                        chains => {
                            commit("chains", chains);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getSegmentations()
                .then(
                        segmentations => {
                            commit("segmentations", segmentations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getMainlands()
                .then(
                        mainlands => {
                            commit("mainlands", mainlands);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getSaleTypes()
                .then(
                        saleTypes => {
                            commit("saleTypes", saleTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getTransferServiceTypes()
                .then(
                        transferServiceTypes => {
                            commit("transferServiceTypes", transferServiceTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getTransferTypes()
                .then(
                        transferTypes => {
                            commit("transferTypes", transferTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getActivityServiceTypes()
                .then(
                        activityServiceTypes => {
                            commit("activityServiceTypes", activityServiceTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getActivitySegmentations()
                .then(
                        activitySegmentations => {
                            commit("activitySegmentations", activitySegmentations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getCommercialAreas()
                .then(
                        comercialareas => {
                            commit("comercialareas", comercialareas);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getAgencyGroups()
                .then(
                        agencygroups => {
                            commit("agencygroups", agencygroups);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getPayments()
                .then(
                        payments => {
                            commit("payments", payments);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getAmadeus()
                .then(
                        amadeus => {
                            commit("amadeus", amadeus);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
        masterSrv.getNumClients()
                .then(
                        numClients => {
                            commit("numClients", numClients);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getMaster({dispatch, commit}, store) {
        masterSrv.getMaster(store)
                .then(
                        master => {
                            commit(store, master);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getServices({dispatch, commit}, env) {
        masterSrv.getServices(env)
                .then(
                        services => {
                            commit("services", services);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getGroups({dispatch, commit}, env) {
        masterSrv.getGroups(env)
                .then(
                        groups => {
                            commit("groups", groups);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getDepths({dispatch, commit}) {
        masterSrv.getDepths()
                .then(
                        depths => {
                            commit("depths", depths);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getPlatforms({dispatch, commit}) {
        masterSrv.getPlatforms()
                .then(
                        platforms => {
                            commit("platforms", platforms);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getSalesModels({dispatch, commit}) {
        masterSrv.getSalesModels()
                .then(
                        salesModels => {
                            commit("salesModels", salesModels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getRateTypes({dispatch, commit}) {
        masterSrv.getRateTypes()
                .then(
                        rateTypes => {
                            commit("rateTypes", rateTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getProductOrigins({dispatch, commit}) {
        masterSrv.getProductOrigins()
                .then(
                        productOrigins => {
                            commit("productOrigins", productOrigins);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getEnvCredential({dispatch, commit}) {
        masterSrv.getEnvCredential()
                .then(
                        envCredential => {
                            commit("envCredential", envCredential);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getDistributionModels({dispatch, commit}) {
        masterSrv.getDistributionModels()
                .then(
                        distributionModels => {
                            commit("distributionModels", distributionModels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getRequests({dispatch, commit}) {
        masterSrv.getRequests()
                .then(
                        requests => {
                            commit("requests", requests);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getLanguages({dispatch, commit}) {
        masterSrv.getLanguages()
                .then(
                        languages => {
                            commit("languages", languages);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getServiceTypes({dispatch, commit}) {
        masterSrv.getServiceTypes()
                .then(
                        serviceTypes => {
                            commit("serviceTypes", serviceTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getSalesChannels({dispatch, commit}) {
        masterSrv.getSalesChannels()
                .then(
                        salesChannels => {
                            commit("salesChannels", salesChannels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getDistributionServices({dispatch, commit}) {
        masterSrv.getDistributionServices()
                .then(
                        distributionServices => {
                            commit("distributionServices", distributionServices);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getSystems({dispatch, commit}) {
        masterSrv.getSystems()
                .then(
                        externalSystems => {
                            commit("externalSystems", externalSystems);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getUsers({dispatch, commit}) {
        masterSrv.getUsers()
                .then(
                        users => {
                            commit("users", users);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getBuyModels({dispatch, commit}) {
        masterSrv.getBuyModels()
                .then(
                        buyModels => {
                            commit("buyModels", buyModels);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getClientPrefs({dispatch, commit}) {
        masterSrv.getClientPrefs()
                .then(
                        clientPrefs => {
                            commit("clientPrefs", clientPrefs);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getTopDestinations({dispatch, commit}) {
        masterSrv.getTopDestinations()
                .then(
                        topDestinations => {
                            commit("topDestinations", topDestinations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getCurrencies({dispatch, commit}) {
        masterSrv.getCurrencies()
                .then(
                        currencies => {
                            commit("currencies", currencies);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getCountries({dispatch, commit}) {
        masterSrv.getCountries()
                .then(
                        countries => {
                            commit("countries", countries);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getCorporations({dispatch, commit}) {
        masterSrv.getCorporations()
                .then(
                        corporations => {
                            commit("corporations", corporations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getInvoiceTypes({dispatch, commit}) {
        masterSrv.getInvoiceTypes()
                .then(
                        invoicetypes => {
                            commit("invoicetypes", invoicetypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getBrands({dispatch, commit}) {
        masterSrv.getBrands()
                .then(
                        brands => {
                            commit("brands", brands);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getClientTypes({dispatch, commit}) {
        masterSrv.getClientTypes()
                .then(
                        clienttypes => {
                            commit("clienttypes", clienttypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getAreas({dispatch, commit}) {
        masterSrv.getAreas()
                .then(
                        areas => {
                            commit("areas", areas);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getSubSupplier({dispatch, commit}) {
        masterSrv.getSubSupplier()
                .then(
                        subsuppliers => {
                            commit("subsuppliers", subsuppliers);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getSysSupplier({dispatch, commit}) {
        masterSrv.getSysSupplier()
                .then(
                        syssuppliers => {
                            commit("syssuppliers", syssuppliers);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getState({dispatch, commit}) {
        masterSrv.getState()
                .then(
                        states => {
                            commit("states", states);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getStatusCredentials({dispatch, commit}) {
        masterSrv.getStatusCredentials()
                .then(
                        statusCredentials => {
                            commit("statusCredentials", statusCredentials);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getChains({dispatch, commit}) {
        masterSrv.getChains()
                .then(
                        chains => {
                            commit("chains", chains);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getSegmentations({dispatch, commit}) {
        masterSrv.getSegmentations()
                .then(
                        segmentations => {
                            commit("segmentations", segmentations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getMainlands({dispatch, commit}) {
        masterSrv.getMainlands()
                .then(
                        mainlands => {
                            commit("mainlands", mainlands);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getSaleTypes({dispatch, commit}) {
        masterSrv.getSaleTypes()
                .then(
                        saleTypes => {
                            commit("saleTypes", saleTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getTransferServiceTypes({dispatch, commit}) {
        masterSrv.getTransferServiceTypes()
                .then(
                        transferServiceTypes => {
                            commit("transferServiceTypes", transferServiceTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getTransferTypes({dispatch, commit}) {
        masterSrv.getTransferTypes()
                .then(
                        transferTypes => {
                            commit("transferTypes", transferTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getActivityServiceTypes({dispatch, commit}) {
        masterSrv.getActivityServiceTypes()
                .then(
                        activityServiceTypes => {
                            commit("activityServiceTypes", activityServiceTypes);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getActivitySegmentations({dispatch, commit}) {
        masterSrv.getActivitySegmentations()
                .then(
                        activitySegmentations => {
                            commit("activitySegmentations", activitySegmentations);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getCommercialAreas({dispatch, commit}) {
        masterSrv.getCommercialAreas()
                .then(
                        comercialareas => {
                            commit("comercialareas", comercialareas);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getAgencyGroups({dispatch, commit}) {
        masterSrv.getAgencyGroups()
                .then(
                        agencygroups => {
                            commit("agencygroups", agencygroups);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getPayments({dispatch, commit}) {
        masterSrv.getPayments()
                .then(
                        payments => {
                            commit("payments", payments);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getAmadeus({dispatch, commit}) {
        masterSrv.getAmadeus()
                .then(
                        amadeus => {
                            commit("amadeus", amadeus);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    },
    getNumClients({dispatch, commit}) {
        masterSrv.getNumClients()
                .then(
                        numClients => {
                            commit("numClients", numClients);
                        },
                        error => {
                            commit('clear', error);
                            dispatch('alert/error', error, {root: true});
                        }
                );
    }
};

const mutations = {
    business_app(state, business_app) {
        state.business_app = business_app;
    },
    languages_app(state, languages_app) {
        state.languages_app = languages_app;
    },
    company(state, companies) {
        state.companies = companies;
    },
    environment(state, environments) {
        state.environments = environments;
    },
    services(state, services) {
        state.services = services;
    },
    groups(state, groups) {
        state.groups = groups;
    },
    depths(state, depths) {
        state.depths = depths;
    },
    platforms(state, platforms) {
        state.platforms = platforms;
    },
    salesModels(state, salesModels) {
        state.salesModels = salesModels;
    },
    rateTypes(state, rateTypes) {
        state.rateTypes = rateTypes;
    },
    productOrigins(state, productOrigins) {
        state.productOrigins = productOrigins;
    },
    distributionModels(state, distributionModels) {
        state.distributionModels = distributionModels;
    },
    requests(state, requests) {
        state.requests = requests;
    },
    envCredential(state, envCredential) {
        state.envCredential = envCredential;
    },
    languages(state, languages) {
        state.languages = languages;
    },
    serviceTypes(state, serviceTypes) {
        state.serviceTypes = serviceTypes;
    },
    salesChannels(state, salesChannels) {
        state.salesChannels = salesChannels;
    },
    distributionServices(state, distributionServices) {
        state.distributionServices = distributionServices;
    },
    externalSystems(state, externalSystems) {
        state.externalSystems = externalSystems;
    },
    users(state, users) {
        state.users = users;
    },
    buyModels(state, buyModels) {
        state.buyModels = buyModels;
    },
    clientPrefs(state, clientPrefs) {
        state.clientPrefs = clientPrefs;
    },
    topDestinations(state, topDestinations) {
        state.topDestinations = topDestinations;
    },
    currencies(state, currencies) {
        state.currencies = currencies;
    },
    countries(state, countries) {
        state.countries = countries;
    },
    corporations(state, corporations) {
        state.corporations = corporations;
    },
    invoicetypes(state, invoicetypes) {
        state.invoicetypes = invoicetypes;
    },
    brands(state, brands) {
        state.brands = brands;
    },
    clienttypes(state, clienttypes) {
        state.clienttypes = clienttypes;
    },
    areas(state, areas) {
        state.areas = areas;
    },
    subsuppliers(state, subsuppliers) {
        state.subsuppliers = subsuppliers;
    },
    syssuppliers(state, syssuppliers) {
        state.syssuppliers = syssuppliers;
    },
    states(state, states) {
        state.states = states;
    },
    statusCredentials(state, statusCredentials) {
        state.statusCredentials = statusCredentials;
    },
    chains(state, chains) {
        state.chains = chains;
    },
    segmentations(state, segmentations) {
        state.segmentations = segmentations;
    },
    mainlands(state, mainlands) {
        state.mainlands = mainlands;
    },
    saleTypes(state, saleTypes) {
        state.saleTypes = saleTypes;
    },
    transferServiceTypes(state, transferServiceTypes) {
        state.transferServiceTypes = transferServiceTypes;
    },
    transferTypes(state, transferTypes) {
        state.transferTypes = transferTypes;
    },
    activityServiceTypes(state, activityServiceTypes) {
        state.activityServiceTypes = activityServiceTypes;
    },
    activitySegmentations(state, activitySegmentations) {
        state.activitySegmentations = activitySegmentations;
    },
    comercialareas(state, comercialareas) {
        state.comercialareas = comercialareas;
    },
    agencygroups(state, agencygroups) {
        state.agencygroups = agencygroups;
    },
    payments(state, payments) {
        state.payments = payments;
    },
    amadeus(state, amadeus) {
        state.amadeus = amadeus;
    },
    numClients(state,numClients) {
        state.numClients = numClients;
    },
    clear(state) {
        state.languages_app = null;
        state.syssuppliers = null;
        state.companies = null;
        state.environments = null;
        state.services = null;
        state.groups = null;
        state.depths = null;
        state.platforms = null;
        state.salesModels = null;
        state.rateTypes = null;
        state.productOrigins = null;
        state.distributionModels = null;
        state.requests = null;
        state.envCredential = null;
        state.languages = null;
        state.serviceTypes = null;
        state.salesChannels = null;
        state.distributionServices = null;
        state.externalSystems = null;
        state.users = null;
        state.buyModels = null;
        state.clientPrefs = null;
        state.topDestinations = null;
        state.currencies = null;
        state.countries = null;
        state.corporations = null;
        state.invoiceTypes = null;
        state.brands = null;
        state.clienttypes = null;
        state.areas = null;
        state.subsuppliers = null;
        state.states = null;
        state.statusCredentials = null;
        state.chains = null;
        state.segmentations = null;
        state.mainlands = null;
        state.saleTypes = null;
        state.transferServiceTypes = null;
        state.transferTypes = null;
        state.activityServiceTypes = null;
        state.activitySegmentations = null;
        state.comercialareas = null;
        state.agencygroups = null;
        state.payments = null;
        state.amadeus = null;
        state.numClients = null;
    }
};

export const master = {
    namespaced: true,
    state,
    actions,
    mutations
};
