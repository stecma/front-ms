const state = {
	type: null,
	message: null,
	dialog: false,
};

const actions = {
	success({commit}, message) {
		commit('success', message);
	},
	error({commit}, message) {
		commit('error', message);
	},
	clear({commit}, message) {
		commit('clear', message);
	}
};

const mutations = {
	success(state, message) {
		state.type = 'success';
		state.message = message;
		state.dialog = true;
	},
	error(state, message) {
		state.type = 'error';
		state.message = message;
		state.dialog = true;
	},
	clear(state) {
		state.type = null;
		state.message = null;
		state.dialog = false;
	}
};

export const alert = {
	namespaced: true,
	state,
	actions,
	mutations
};
