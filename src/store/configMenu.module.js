import { router } from '@router';

const state = {
	business: null,
	enviroment: null,
	company: null,
	menu: null,
};

const actions = {
	reloadMenu({ commit }) {
		let user = JSON.parse(localStorage.getItem('user'));
		if (user && user.configMenu) {
			if (!state.business) {
				commit('assignBusiness', user.bus[0]);
			}
			if (!state.company) {
				commit('assignCia', state.business.masters[0].id);
			}
			if (!state.enviroment) {
				commit('assignEnv', process.env.VUE_APP_ENV);
			}
			for (const cfgMenu of user.configMenu) {
				if (cfgMenu.env === state.enviroment) {
					/* En cualquier caso buscamos que la compañia exista */
					if (cfgMenu.cia === state.company) {
						/* Limpiamos los servicios no activos */
						for (let m = 0; m < cfgMenu.menus.length; m++) {
							/* Limpiamos los menus no activos */
							if (!cfgMenu.menus[m].act) {
								cfgMenu.menus.splice(m, 1);
								m--;
							} else {
								let count = 0;
								/* Limpiamos los servicios no activos de un menu */
								for (let s = 0; Object.prototype.hasOwnProperty.call(cfgMenu.menus[m], 'srv') && s < cfgMenu.menus[m].srv.length; s++) {
									if (!cfgMenu.menus[m].srv[s].act) {
										cfgMenu.menus[m].srv.splice(s, 1);
										s--;
									} else {
										count++;
									}
								}
								/* Limpiamos los grupos no activos de un menu */
								for (let g = 0; Object.prototype.hasOwnProperty.call(cfgMenu.menus[m], 'grp') && g < cfgMenu.menus[m].grp.length; g++) {
									if (!cfgMenu.menus[m].grp[g].act) {
										cfgMenu.menus[m].grp.splice(g, 1);
										g--;
									} else {
										for (let s = 0; Object.prototype.hasOwnProperty.call(cfgMenu.menus[m].grp[g], 'srv') && s < cfgMenu.menus[m].grp[g].srv.length; s++) {
											if (!cfgMenu.menus[m].grp[g].srv[s].act) {
												cfgMenu.menus[m].grp[g].srv.splice(s, 1);
												s--;
											} else {
												count++;
											}
										}
									}
								}

								/* Limpiamos el menu si no tiene elementos visibles */
								if (count === 0) {
									cfgMenu.menus.splice(m, 1);
									m--;
								}
							}
						}
						commit('assignMenu', cfgMenu.menus);
						break;
					}
				}
			}
		} else {
			return {};
		}
		// noinspection JSIgnoredPromiseFromCall
		router.push('/');
	},
	assignBusiness({ commit }, business) {
		commit('assignBusiness', business);
	},
	assignCia({ dispatch, commit }, cia) {
		commit('assignCia', cia);
		commit('clearMenu');
		dispatch('reloadMenu');
	},
	assignEnv({ dispatch, commit }, env) {
		commit('assignEnv', env);
		commit('clearMenu');
		dispatch('reloadMenu');
	}
};

const mutations = {
	assignBusiness(state, newBusiness) {
		state.business = newBusiness;
	},
	assignEnv(state, newEnv) {
		state.enviroment = newEnv;
	},
	assignCia(state, newCia) {
		state.company = newCia;
	},
	assignMenu(state, newMenu) {
		state.menu = newMenu;
	},
	clearMenu(state) {
		state.menu = null;
	}
};

export const configMenu = {
	namespaced: true,
	state,
	actions,
	mutations
};
