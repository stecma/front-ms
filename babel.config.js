module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
    '@babel/preset-env', 
  ],
  "plugins": ["@babel/plugin-proposal-object-rest-spread"]
}
