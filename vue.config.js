const path = require('path')

function resolveRealPath(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  chainWebpack: config => {
    config.resolve.alias
      .set('@assets', resolveRealPath('src/assets'))
      .set('@components', resolveRealPath('src/components'))
      .set('@helper', resolveRealPath('src/helpers'))
      .set('@layouts', resolveRealPath('src/layouts'))
      .set('@pages', resolveRealPath('src/pages'))
      .set('@router', resolveRealPath('src/router'))
      .set('@services', resolveRealPath('src/services'))
      .set('@store', resolveRealPath('src/store'))


  },

  devServer: {
    port: 8082,
  },

  lintOnSave: undefined,
  "transpileDependencies": [
    "vuetify"
  ]
}