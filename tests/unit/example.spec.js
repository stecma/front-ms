import { shallowMount } from '@vue/test-utils'
import TheFooter from '@/components/TheFooter.vue'

describe('TheFooter.vue', () => {
  it('check text of footer', () => {
    const msg = '@PLEXUS 2019'
    const wrapper = shallowMount(TheFooter)
    expect(wrapper.find('.text-center').text()).toBe(msg)
  })
})
